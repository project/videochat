
CONTENTS OF THIS FILE
-----------------------------------------------------------------------------------------
 * Introduction
 * Installation
 * Usage
  
INTRODUCTION
-----------------------------------------------------------------------------------------
Before you install the videochat module, please read and agree to these conditions as can be found at vagipe.com:

    * You may modify the login form, build your own opener mechanism, or even integrate the chat with your own database.
    * You may only call the chat through its wrapper page (webcam_window_ext.php).
    * You may not download the wrapper page or the flash file or install either one on your own server.
    * Please understand that there is no warranty whatsoever for this service.
    * We reserve the right to exclude anyone from this service without notice.
    * You are responsible for keeping your rooms content in line with your local laws.


INSTALLATION
-----------------------------------------------------------------------------------------
1. Copy the entire videochat module directory into your normal directory for modules, 
   usually sites/all/modules.
   
2. Enable the Video Chat module at http://www.example.com/?q=admin/build/modules (or http://www.example.com/admin/build/modules if clean URLs are enabled).

3. Enable the Video Chat block in the region of your choice at http://www.example.com/?q=admin/build/block (or http://www.example.com/admin/build/block if clean URLs are enabled).

USAGE
-----------------------------------------------------------------------------------------
 * This module will provide a block on your site with a submit button in it that says, "Enter Video Chatroom" -- access to this block is restricted by default to user #1 (the admin user).  It will also provide a form on the user registration page that will reject any registration submissions where the user has not agreed to the terms as set forth on that form.
 * To allow authenticated and/or anonymous users to see the block, set the permissions accordingly at admin/user/permissions.
 * If the current user is authenticated (registered), clicking on the button will automatically populate the "user name" in the URL with the user's registered username ($user->name); if the user is an anonymous (non-registered) user, the "user name" part will instead be filled with random letters and numbers, based on an md5 of the user's IP address.  Either way, the "room id" will automatically be populated with "example.com" (the "www." will be stripped from the URL of your site).
 * This module was written and published with the express written consent of the provider (vagipe.com) with such consent being granted to the module's author on July 13, 2009.
 * Behave.  :)